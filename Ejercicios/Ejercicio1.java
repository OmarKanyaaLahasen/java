/*
Nombre: Omar Kanyaa Lahasen
Programa: Ejercicio1
*/


/*Inicio del programa*/
public class Ejercicio1{
	public static void main (String[]args){
		/*Comprobar en la linea de comandos si se ha introducido dos numeros*/
		if(args.length != 3){
            System.out.println("Porfavor, introduzca tres numeros");
            System.exit(0);
        }

        /*Asignar las variables*/
        int numero1, numero2, numero3;
        numero1= Integer.parseInt(args[0]);
        numero2= Integer.parseInt(args[1]);
        numero3= Integer.parseInt(args[2]);
     	
     	/*Asignamos las reglas*/
     	if(numero1>numero2 && numero1>numero3){
     		System.out.println("El numero "+numero1+" es mayor");
     	}else{
     		System.out.println("El numero "+numero1+" es menor");
     	}

     	if(numero2>numero3 && numero2>numero1){
     		System.out.println("El numero "+numero2+" es mayor");
     	}else{
     		System.out.println("El numero "+numero2+" es menor");
     	}

     	if(numero3>numero2 && numero3>numero1){
     		System.out.println("El numero "+numero3+" es mayor");
     	}else{
     		System.out.println("El numero "+numero3+" es menor");
     	}
	}
}
/*Final del programa*/
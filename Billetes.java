public class Billetes {
	public static void main(String[] args) {
		int cantidad=Integer.parseInt(args[0]);
		int b500, b200, b100, b50, b20, b10, b5, resto;
		b500=cantidad/500;
		resto=cantidad%500;
		b200=cantidad/200;
		resto=cantidad%200;
		b100=cantidad/100;
		resto=cantidad%100;
		b50=cantidad/50;
		resto=cantidad%50;
		b20=cantidad/20;
		resto=cantidad%20;
		b10=cantidad/10;
		resto=cantidad%10;
		b5=cantidad/5;
		resto=cantidad%5;

		if (b500>0) {
			System.out.println("Billetes de 500: "+b500);
		}

		if (b200>0) {
			System.out.println("Billetes de 200: "+b200);
		}

		if (b100>0) {
			System.out.println("Billetes de 100: "+b100);
		}

		if (b50>0) {
			System.out.println("Billetes de 50: "+b50);
		}

		if (b20>0) {
			System.out.println("Billetes de 20: "+b20);
		}

		if (b10>0) {
			System.out.println("Billetes de 10: "+b10);
		}

		if (b5>0) {
			System.out.println("Billetes de 5: "+b5);
		}
		if (resto>0) {
			System.out.println("Sobran "+resto);
		}
	}
}
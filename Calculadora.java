//Import lo que hace es que llama a "Scanner" despues del "java.util" para que se ejecute
import java.util.Scanner;

public class Calculadora {

    public static void main(String args []) {
    	//Este es el titulo del programa
	System.out.println ();
    	System.out.println ("C A L C U L A D O R A");
	System.out.println ();
	System.out.println ("Software Libre");
	System.out.println ();
	System.out.println ("----------------------------------------");
    	//Esta linea sirve para empezar introducir los datos
    	Scanner sc= new Scanner(System.in);
    	//En esta linea muestra en pantalla el texto que esta entre parentesis y en dobles comillas
	System.out.println ();
    	System.out.println ("Introduce el Numero 1");
	System.out.println (); 
   	//En esta linea hacemos que "cadena" deje introducir el primer numero
        double cadena= sc.nextDouble();
        //En esta linea muestra en pantalla el texto que esta entre parentesis y en dobles comillas
	System.out.println ("----------------------------------------");
	System.out.println ();	
        System.out.println ("Introduce el Numero 2");
	System.out.println ();
        //En esta linea hacemos que "num" deje introducir el segundo numero
        double num= sc.nextDouble();
        //En esta linea hacemos int sume las dos variables "cadena" y "num"
		double suma=cadena+num;
    	//En esta linea se suma hacemos que "suma" calcule la "cadena" y el "num"
		System.out.println ("----------------------------------------");
		System.out.println ();		
		System.out.println ("La operacion es igual a: " + suma);
		System.out.println ();
		System.out.println ("----------------------------------------");
	}
}

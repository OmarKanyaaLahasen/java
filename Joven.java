//inicio
public class Joven {
	String nombre;
	String apellido1;
	String apellido2;
	int edad;
	int nivelDeEstudios;
	double ingresos;
	boolean jasp;

	public Joven(String nombre, String apellido1, String apellido2, int edad, int nivelDeEstudios, double ingresos) {		
		//short edad;
		//byte nivelDeEstudios;
		//double ingresos;
		/*(Tanto en el parentesis de la variable de miembro
		como dentro del campo funcionan igual)*/

		/*"this" lo que hace es que hace referencia a "edad" de la
		variable miembro y oculta la variable "edad" que esta al 
		inicio de la clase*/
		this.nombre=nombre;
		this.apellido1=apellido1;
		this.apellido2=apellido2;
		this.edad=edad;
		this.nivelDeEstudios=nivelDeEstudios;
		this.ingresos=ingresos;
	}

	public void establecerJasp() {
		this.jasp=(this.edad<=28 && this.nivelDeEstudios>3 && this.ingresos>28000); 
	}
	public static void main(String[] args) {
		Joven p=new Joven("Pepito","Lopez","Perez", 18, 4, 28000.65); 
		Joven q=new Joven("Luisa","Lopez","Martinez", 15, 2, 0.0);	
		/*"new" sirve para crear una  variable de una clase*/
		/*los datos dentro del parentesis de la linea "pepito" 
		y "juanito" hacen relacion a la linea "public Joven"*/
		p.establecerJasp();
		q.establecerJasp();
		System.out.println(p.jasp);
	}
}
//final